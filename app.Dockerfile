FROM advaice/vertica-vertica_backend-base:latest

WORKDIR /srv/www

# RUN mkdir /root/.aws

# RUN echo "[profile org-access-backend-qa]\n\
# role_arn = arn:aws:iam::084605867719:role/app-qa\n\
# source_profile = default" >> /root/.aws/credentials

# RUN echo "[default]\n\
# region = eu-central-1\n\
# output = json" >> /root/.aws/config

COPY ./ /srv/www/