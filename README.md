# Django Rest Framework + Docker

## Create Django application

    python -m pip install Django
    
    django-admin startproject PROJECT
 
## Install libs

    pipenv lock 
    
    pipenv install