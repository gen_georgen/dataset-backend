# syntax = docker/dockerfile:experimental
FROM python:3.8-slim

WORKDIR /srw/www/

#COPY ./Pipfile /srw/www

RUN apt-get update
RUN apt-get install -y --no-install-recommends gcc python3-dev 
RUN apt-get install -y wget
RUN apt-get install -y xvfb
RUN wget -P /tmp http://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-stable/google-chrome-stable_89.0.4389.72-1_amd64.deb
RUN apt-get install -y /tmp/google-chrome-stable_89.0.4389.72-1_amd64.deb
RUN rm /tmp/google-chrome-stable_89.0.4389.72-1_amd64.deb
RUN apt-get install -y --no-install-recommends libdbus-glib-1-dev

COPY ./drivers/chromedriver /opt
RUN chmod +x /opt/chromedriver

COPY ./requirements.txt /srw/www

# Experimental Docker mode (mount dir)
RUN --mount=type=cache,target=/root/.cache/pip pip install -r requirements.txt
RUN python -m playwright install
#RUN pip install pipenv

#RUN pipenv lock
#RUN pipenv install --system --dev
