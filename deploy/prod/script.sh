#!/bin/sh
ssh -tt alphapeas@$PROD_SERVER "
docker login -u advaice -p $DOCKER_HUB_PASSWORD &&
docker pull advaice/$PROJECT_NAME-$CI_PROJECT_NAME:$CI_COMMIT_BRANCH &&
docker-compose up -d
docker exec -it backend python manage.py migrate
docker image prune -f
"

#ansible-playbook -i $QA_SERVER, -b -u alphapeas -e project_environment=dev playbook.yml