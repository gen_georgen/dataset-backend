"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

from .yasg import urlpatterns as doc_urls
# from app.api.v1.urls import urlpatterns as api_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('ckeditor/', include('ckeditor_uploader.urls')),
    url('api-auth/', include('rest_framework.urls')),
    # url('auth/', include('djoser.urls')),
    url('auth/', include('djoser.urls.jwt')),
    # url('', include(api_urls)),
    path('', include('django_prometheus.urls')),
]

# urlpatterns += [
#     path('django-rq/', include('django_rq.urls'))
# ]

# urlpatterns += static(settings.STATIC_ROOT, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

urlpatterns += doc_urls
