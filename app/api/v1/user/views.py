from django.utils.translation import gettext_lazy as _
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import ValidationError

from djoser.compat import get_user_email
from djoser.views import UserViewSet
from djoser.conf import settings


class CustomUserViewSet(UserViewSet):
    @action(["post"], detail=False)
    def reset_password(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.get_user()

        if user:
            context = {"user": user}
            to = [get_user_email(user)]
            settings.EMAIL.password_reset(self.request, context).send(to)
        else:
            raise ValidationError({"detail": _("User with this email not found")})

        return Response(status=status.HTTP_204_NO_CONTENT)