from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Register your models here.
from app.user.models import UserModel


class UserModelAdmin(UserAdmin):
    pass


admin.site.register(UserModel, UserModelAdmin)
