import os

from django.contrib.auth.tokens import default_token_generator
from templated_mail.mail import BaseEmailMessage
from django.conf import settings as django_settings

from djoser import utils
from djoser.conf import settings
from ..aws.providers import SES


class PasswordResetEmail(BaseEmailMessage):
    template_name = os.path.join(django_settings.TEMPLATES_ROOT, "email/password_reset.html")

    def get_context_data(self):
        # PasswordResetEmail can be deleted
        context = super().get_context_data()

        user = context.get("user")
        # SES.send_message(os.getenv('EMAILS_FROM_EMAIL'), user.email, context)
        context["uid"] = utils.encode_uid(user.pk)
        context["token"] = default_token_generator.make_token(user)
        context["url"] = "auth/reset-password/" + settings.PASSWORD_RESET_CONFIRM_URL.format(**context)
        return context


class PasswordChangedConfirmationEmail(BaseEmailMessage):
    template_name = os.path.join(django_settings.TEMPLATES_ROOT, "email/password_changed_confirmation.html")
