from django.db import models
from django.contrib.auth import get_user
from django.utils.translation import gettext_lazy as _
from app.user.models import UserModel

class TaskModel(models.Model):

    DONE = 0
    ASSIGNED = 1
    CREATED = 2
    
    STATUSES = (
        (DONE, _('Done')),
        (ASSIGNED, _('Assigned')),
        (CREATED, _('Created')),
    )
    name = models.CharField(max_length=100)
    user = models.ForeignKey(UserModel, on_delete=models.PROTECT, related_name="tasks")
    comments = models.CharField(max_length=500, null=True, blank=True)
    status = models.IntegerField(choices=STATUSES, default=CREATED)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)



class LabelModel(models.Model):

    DENIAL = 0
    ANGER = 1
    BARGAINING = 2
    DEPRESSION = 3
    ACCEPTANCE = 5
    
    EMOJI = (
        (DENIAL, _('Denial')),
        (ANGER, _('Anger')),
        (BARGAINING, _('Bargaining')),
        (DEPRESSION, _('Depression')),
        (ACCEPTANCE, _('Acceptance')),
    )

    emoji = models.IntegerField(choices=EMOJI)
    left_up_x = models.IntegerField()
    left_up_y = models.IntegerField()
    right_bot_x = models.IntegerField()
    right_bot_y = models.IntegerField()


class ImageModel(models.Model):
    task = models.ForeignKey(TaskModel, on_delete=models.PROTECT, related_name="images")
    created_at = models.DateTimeField(auto_now_add=True)
    file = models.ImageField(upload_to='uploads/%Y/%m/%d/')
    label = models.ForeignKey(LabelModel, on_delete=models.PROTECT, related_name="images")