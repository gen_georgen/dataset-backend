from django.apps import AppConfig


class AppConfig(AppConfig):
    name = 'app.article'

    def ready(self):
        import app.article.signals
