from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator

from app.notification.consumers import CreativeConsumer, CreativeProcessConsumer, NotificationConsumer

application = ProtocolTypeRouter({
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter([
                url(r"creative/(?P<creative_id>[0-9]*)/external", CreativeConsumer),
                url(r"creative/processing", CreativeProcessConsumer),
                url(r"notification", NotificationConsumer)
            ])
        )
    )
})
